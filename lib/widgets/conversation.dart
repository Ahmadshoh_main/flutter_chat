import 'dart:async';

import 'package:chat_bot/models/message_model.dart';

import '../app_theme.dart';
import 'package:flutter/material.dart';

class Conversation extends StatefulWidget {
  final List<Message> messages;

  const Conversation({
    Key? key,
    required this.messages,
  }) : super(key: key);

  @override
  State<Conversation> createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  @override
  Widget build(BuildContext context) {

    return ListView.builder(
      reverse: true,
      itemCount: widget.messages.length,
      itemBuilder: (context, int index) {
        final message = widget.messages[index];
        return Container(
          margin: const EdgeInsets.only(top: 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment:
                message.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (!message.isMe)
                    const CircleAvatar(
                      radius: 15,
                      backgroundImage: AssetImage('assets/chatbot_avatar.png'),
                    ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),

                    constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6),
                    decoration: BoxDecoration(
                      color: message.isMe ? MyTheme.kAccentColor : Colors.grey[200],
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(16),
                        topRight: const Radius.circular(16),
                        bottomLeft: Radius.circular(message.isMe ? 12 : 0),
                        bottomRight: Radius.circular(message.isMe ? 0 : 12),
                      )
                    ),
                    child: Text(
                      message.text,
                      style: MyTheme.bodyTextMessage.copyWith(
                        color: message.isMe ? Colors.white : Colors.grey[800]
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment:
                  message.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
                  children: [
                    if (!message.isMe) const SizedBox(width: 40),

                    const SizedBox(width: 8),

                    Text(
                      message.time,
                      style: MyTheme.bodyTextTime,
                    )
                  ],
                ),
              )
            ],
          ),
        );
      }
    );
  }
}