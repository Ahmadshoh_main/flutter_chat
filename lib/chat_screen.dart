import 'dart:math';

import 'package:chat/chat.dart';
import 'package:chat_bot/models/message_model.dart';
import 'package:chat_bot/widgets/conversation.dart';
import 'package:flutter/services.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';

import '../app_theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ChatScreen extends StatefulWidget {

  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();

  List<Message> messages = [];
  List<String> test = [];

  List<String> greetings = [
    "Grüße. Womit kann ich Ihnen behilflich sein?",
    "Hallo lieber Kunde. Ich stehe Ihnen voll und ganz zur Verfügung.",
    "Hallo. Ich bin der klügste Chatbot der Welt. Stellen Sie mir jede Frage und die Antworten werden Sie überraschen."
  ];

  void sendMessage() {
    generateMessage(messageTextController.text, isMe: true);

    getAnswer();

    messageTextController.clear();
  }


  void getAnswer() async {
    final messageText = messageTextController.text;

    if (ratio(messageText, "Hallo") > 50 ||ratio(messageText, "Hi") > 50) {
      startChatBot();
    } else if (ratio(messageText, "System info") > 50 || ratio(messageText, "Hardware info") > 50) {
      Stream<List<String>> answers = Chat.systemInfo();

      answers.listen((event) {

        for (var answer in event) {
          generateMessage(answer);
        }
      });

    } else if (ratio(messageText, "Erzähl bitte einen Witz") > 50 || messageText.contains("Witz") || messageText.contains("Joke")) {

      final answer = await Chat.getJoke();
      generateMessage(answer);

    } else if (ratio(messageText, "Wer bist du") > 50 || messageText.contains("Platform") || messageText.contains("Phone")) {

      final answer = await Chat.getPlatform();
      generateMessage(answer);

    }else if (ratio(messageText, "Wie viel Uhr ist es") > 50 || messageText.contains("Uhr") || messageText.contains("Time")) {

      final answer = await Chat.getTime();
      generateMessage(answer);

    } else if (ratio(messageText, "Wo bist du") > 50) {

      final answer = await Chat.getLocation();
      generateMessage(answer);

    } else if (ratio(messageText, "Wo kannst du für mich machen") > 50) {

      final answer = await Chat.getServices();
      generateMessage(answer);

    } else {
      generateMessage("Es tut mir leid. Ich konnte dich nicht verstehen. Mir wurde noch nicht beigebracht, so zu sprechen.");
    }
  }

  void generateMessage(String text, {bool isMe = false}) {
    final message = Message(
        time: DateFormat("dd MMM hh:m:ss").format(DateTime.now()),
        text: text,
        isMe: isMe
    );

    setState(() => messages.insert(0, message));
  }

  void startChatBot() {
    final random = Random();
    var i = random.nextInt(greetings.length);

    final startMessage = Message(
        time: DateFormat("dd MMM hh:m:ss").format(DateTime.now()),
        text: greetings[i],
        isMe: false
    );

    setState(() => messages.insert(0, startMessage));
  }

  @override
  void initState() {
    super.initState();

    Chat.locationTest();

    startChatBot();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100,
        centerTitle: false,
        title: Row(
          children: [
            const CircleAvatar(
              radius: 30,
              backgroundImage: AssetImage('assets/chatbot_avatar.png'),
            ),

            const SizedBox(width: 20),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'ChatBot',
                  style: MyTheme.chatSenderName,
                ),
                Text(
                  'online',
                  style: MyTheme.bodyText1.copyWith(fontSize: 18, color: Colors.white),
                ),
              ],
            ),
          ],
        ),
        elevation: 0,
      ),

      backgroundColor: const Color.fromRGBO(43, 152, 240, 1),

      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },

        child: Column(
          children: [
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  child: Conversation(messages: messages),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              color: Colors.white,
              height: 100,
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 14),
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.emoji_emotions_outlined,
                            color: Colors.grey[500],
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: TextField(
                              textInputAction: TextInputAction.go,
                              onSubmitted: (value) => sendMessage(),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Type your message ...',
                                hintStyle: TextStyle(color: Colors.grey[500]),
                              ),
                              controller: messageTextController,
                            ),
                          ),
                          IconButton(
                            padding: const EdgeInsets.all(0),
                            color: Colors.grey[500],
                            icon: const Icon(Icons.attach_file),
                            onPressed: () {
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  GestureDetector(
                    onTap: () => sendMessage(),
                    child: const CircleAvatar(
                      backgroundColor: Color.fromRGBO(43, 152, 240, 1),
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}