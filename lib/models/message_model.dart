class Message {
  final String? avatar;
  final String time;
  final int? unreadCount;
  final bool? isRead;
  final String text;
  final bool isMe;

  Message({
    this.avatar,
    required this.time,
    this.unreadCount,
    required this.text,
    this.isRead,
    required this.isMe
  });
}