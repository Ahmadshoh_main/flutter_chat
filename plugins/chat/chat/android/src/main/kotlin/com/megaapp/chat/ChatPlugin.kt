package com.megaapp.chat

import androidx.annotation.NonNull
import android.os.Handler

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.EventChannel
import java.util.*

/** ChatPlugin */
class ChatPlugin: FlutterPlugin, MethodCallHandler {

  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "chat")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "platform" => {

      }

      "system_info" => {

      }
      else -> result.notImplemented()
    }
  }
  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}

class BotAnswer: EventChannel.StreamHandler {
  var sink: EventChannel.EventSink? = null
  var handler: Handler? = null

  private val runnable = Runnable {
    sendBotAnswer()
  }

  fun sendBotAnswer() {
    val randomNumber = Random().nextInt(9)
    sink?.success(randomNumber)
    handler?.postDelayed(runnable, 1000)
  }

  override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
    sink = events
    handler = Handler()
    handler?.post(runnable)
  }

  override fun onCancel(arguments: Any?) {
    sink = null
    handler?.removeCallbacks(runnable)
  }
}