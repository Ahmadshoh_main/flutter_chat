import Flutter
import UIKit
import Foundation
import CoreLocation

protocol SwiftChatPluginHandleProtocol {
    var plugin: SwiftChatPlugin { get }

    func getSystemInfo() -> String
    func getPlatform() -> String
    func getLocation()
}

public class SwiftChatPlugin: NSObject, FlutterPlugin {

  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "chat", binaryMessenger: registrar.messenger())

    let instance = SwiftChatPlugin()

    instance.channel = channel

    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  var channel: FlutterMethodChannel?

  struct FlutterAction {
      var call: FlutterMethodCall
      var callback:FlutterResult
  }

  var actions = [FlutterAction]()

  var resultCallback: FlutterResult?

  var handler: SwiftChatPluginHandleProtocol?

  enum CallMethod:String {
      case getPlatform
      case getSystemInfo
      case getLocation
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {

    actions.append(FlutterAction(call: call, callback: result))
    resultCallback = result

    if handler == nil {
        self.handler = ItonHandler(withPlugin: self)
    }

    guard let h = handler else {
        return
    }

    let method = CallMethod(rawValue: call.method)
    
    switch method {
        case .getPlatform:
          result("iOS " + UIDevice.current.systemVersion)
        case .getSystemInfo:
          channel?.invokeMethod("getSystemInfo", arguments: h.getSystemInfo())
        case .getLocation:
            channel?.invokeMethod("getLocation", arguments: h.getLocation())
        default:
          result(FlutterMethodNotImplemented)
    }
  }
}


class ItonHandler: NSObject, SwiftChatPluginHandleProtocol, CLLocationManagerDelegate {
    var plugin: SwiftChatPlugin

    init(withPlugin p: SwiftChatPlugin) {
        self.plugin = p
    }

    func getSystemInfo() -> String {
        let OS = UIDevice.current.name + " " + UIDevice.current.systemVersion
        let model = UIDevice.current.model
        let hardwareName = UIDevice.current.name
        
        let json: [Any] = [
            ["message": "OS: " + OS],
            ["message": "model: " + model],
            ["message": "hardwareName: " + hardwareName],
        ]
        
        return jsonToString(json: json)
    }
    
    func getPlatform() -> String {
        print("platfor function")
        return "Platofrm";
    }

    func getLocation() {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        print("Hello world")

        // Request a user’s location once
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) -> String {
        guard let location = locations.first else {
            return "Error"
        }
        
        print("\(location.coordinate.latitude) | \(location.coordinate.longitude)")
        return "\(location.coordinate.latitude) | \(location.coordinate.longitude)"
        
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) -> String {
        return "Etwas falsch gegangen"
    }
    
    func jsonToString(json: Any) -> String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string// <-- here is ur string
            return convertedString ?? "Error"

        } catch let myJSONError {
            print(myJSONError)
            return "Error"
        }
    }
}
