
import 'dart:async';
import 'dart:convert';
import 'package:chat_platform_interface/chat_platform_interface.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';


class Chat {

  static Future getPlatform() async {
    return await ChatPlatform.instance.getPlatform();
  }

  static Future getJoke() async {
    final response = await http.get(Uri.parse('https://v2.jokeapi.dev/joke/Any?lang=de&format=txt'));
    return response.body;
  }

  static Future getTime() async {
    return DateFormat('hh:ii:ss').format(DateTime.now());
  }

  static Future getLocation() async {
    return "Es tut mir leid. Aber im Moment kann ich meinen Standort nicht erkennen. Aber Sie können sicher sein, dass mein Standort dort ist, wo Sie sind.";
  }

  static Future getServices() async {
    return "Ich bin der klügste Chatbot der Welt. Stellen Sie mir jede Frage und die Antworten werden Sie überraschen.";
  }

  static Stream<String> locationTest() {
    ChatPlatform.instance.getLocation();

    return ChatPlatform.instance.getLocation().map((json) {
      return "sadfsadf";
    });
  }

  static Stream<List<String>> systemInfo() {
    return ChatPlatform.instance.getSystemInfo().map((json) {
      List info = jsonDecode(json);
      return info.map((event) => event["message"].toString()).toList();
    });
  }
}