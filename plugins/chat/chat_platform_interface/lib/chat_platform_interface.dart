import 'package:chat_platform_interface/method_channel_chat.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

abstract class ChatPlatform extends PlatformInterface {
  ChatPlatform() : super(token: _token);

  static final Object _token = Object();

  static ChatPlatform _instance = MethodChannelChat();

  static ChatPlatform get instance => _instance;

  static set instance(ChatPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String> getPlatform() async {
    throw UnimplementedError("getPlatform() has not been implemented.");
  }


  Stream<String> getSystemInfo() async* {
    throw UnimplementedError("getSystemInfo() has not been implemented.");
  }

  Stream<String> getLocation() async* {
    throw UnimplementedError("getLocation() has not been implemented.");
  }
}