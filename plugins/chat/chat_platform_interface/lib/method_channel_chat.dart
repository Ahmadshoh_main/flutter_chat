import 'dart:async';

import 'package:chat_platform_interface/chat_platform_interface.dart';
import 'package:flutter/services.dart';

const MethodChannel _channel = MethodChannel('chat');

class MethodChannelChat extends ChatPlatform {
  StreamController<MethodCall> _methodCallController = StreamController.broadcast();

  Stream<MethodCall> get _methodStream => _methodCallController.stream;

  MethodChannelChat() {
    _channel.setMethodCallHandler((call) async => _methodCallController.add(call));
  }

  Future<String> getPlatform() {
    return _channel.invokeMethod('getPlatform').then((value) => value ?? null);
  }

  Stream<String> getSystemInfo() async* {
    _channel.invokeMethod('getSystemInfo');

    yield*
      _methodStream.where((event) => event.method == "getSystemInfo")
      .map((event) {
        return event.arguments;
      })
      .map((event) {
        return event;
      });
  }

  Stream<String> getLocation() async* {

    _channel.invokeMethod("getLocation");

    yield*
    _methodStream.where((event) => event.method == "getLocation")
    .map((event) {
      return event.arguments;
    }).map((event) {
      return event;
    });
  }
}